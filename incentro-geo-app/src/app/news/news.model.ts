export class newsitem {
  date: string;
  time: string;
  firstName: string;
  lastName: string;
  newsSort: string;
  nameClient: string;
  newsDescription: string;
  numberReactions: number;
  dateOfEvent: string;
  timeOfEvent: string;

  constructor(date: string, time: string, firstName: string, lastName: string, newsSort: string, nameClient: string,
              newsDescription: string, numberReactions: number, dateOfEvent: string, timeOfEvent: string)
  {
    this.date = date;
    this.time = time;
    this.firstName = firstName;
    this.lastName = lastName;
    this.newsSort = newsSort;
    this.nameClient = nameClient;
    this.newsDescription = newsDescription;
    this.numberReactions = numberReactions;
    this.dateOfEvent = dateOfEvent;
    this.timeOfEvent = timeOfEvent;
  }
}

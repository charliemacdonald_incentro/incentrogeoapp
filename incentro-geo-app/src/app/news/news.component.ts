import {Component, OnInit} from '@angular/core';
import {newsitem} from './news.model';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  newsitems: newsitem[] = [
    new newsitem ('10-04-2018', '12:22', 'Charlie', 'Mac Donald',
    'Update', 'ANWB', 'Voorjaarsborrel bij ANWB!', 5,
      '000000', '0000'),
    new newsitem ('11-04-2018', '13:42', 'Charlie', 'Mac Donald',
      'Question', 'ANWB', 'Voorjaarsborrel bij ANWB!', 2,
      '000000', '0000'),
    new newsitem ('12-04-2018', '12:22', 'Charlie', 'Mac Donald',
      'Question', 'SNS Bank', 'Wie is er nog meer bij SNS?', 1,
      '000000', '0000'),
    new newsitem ('13-04-2018', '12:22', 'Charlie', 'Mac Donald',
      'Event', 'SNS Bank', 'Voorjaarsborrel bij SNS!', 8,
      '20-04-2018', '18:00'),
    new newsitem ('14-04-2018', '12:22', 'Charlie', 'Mac Donald',
      'Event', 'Aegon', 'Voorjaarsborrel bij ANWB!', 3,
      '20-05-2018', '20:00'),
    new newsitem ('15-04-2018', '12:22', 'Charlie', 'Mac Donald',
      'Update', 'Aegon', 'Voorjaarsborrel bij ANWB!', 3,
      '000000', '0000'),
    new newsitem ('19-04-2018', '12:22', 'Charlie', 'Mac Donald',
      'Update', 'Aegon', 'Voorjaarsborrel bij ANWB!', 5,
      '000000', '0000'),
    new newsitem ('20-04-2018', '12:22', 'Charlie', 'Mac Donald',
      'Question', 'SNS Bank', 'Voorjaarsborrel bij ANWB!', 4,
      '000000', '0000'),
    new newsitem ('21-04-2018', '12:22', 'Charlie', 'Mac Donald',
      'Event', 'SNS Bank', 'Voorjaarsborrel bij ANWB!', 7,
      '10-06-2018', '20:30'),
    new newsitem ('22-04-2018', '12:22', 'Charlie', 'Mac Donald',
      'Update', 'ANWB', 'Voorjaarsborrel bij ANWB!', 17,
      '000000', '0000')
  ];
  constructor() { }

  ngOnInit() {
  }

}

import {Component, Input, OnInit} from '@angular/core';
import {newsitem} from '../news.model';

@Component({
  selector: 'app-news-item',
  templateUrl: './news-item.component.html',
  styleUrls: ['./news-item.component.css']
})
export class NewsItemComponent implements OnInit {

  @Input() newsitem: newsitem;

  constructor() { }

  ngOnInit() {
  }

}
